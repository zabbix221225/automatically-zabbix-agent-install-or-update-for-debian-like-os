#!/bin/bash
# Check operation system
VERSION=$(grep -oP '(?<=^VERSION_ID=).+' /etc/os-release | tr -d '"')
# Check operation system version
OS=$(grep -oP '(?<=^ID=).+' /etc/os-release | tr -d '"')
read -p 'IP address or DNS name Zabbix server: ' zabbix_server
# The Zabbix version will be installed
zabbix_version=6.4 #Put here zabbix agent version

# Remove the old zabbix_agent installed
        service zabbix-agent stop
        service zabbix-agent2 stop
        apt remove zabbix* -y
        apt purge zabbix* -y
        rm -r /etc/zabbix

# For Ubuntu OS
if [ "$OS" = "ubuntu" ]
then
        wget https://repo.zabbix.com/zabbix/"$zabbix_version"/"$OS"/pool/main/z/zabbix-release/zabbix-release_"$zabbix_version"-1%2B"$OS""$VERSION"_all.deb

#For Debian OS
elif [ "$OS" = "debian" ]
then
        wget https://repo.zabbix.com/zabbix/"$zabbix_version"/"$OS"/pool/main/z/zabbix-release/zabbix-release_"$zabbix_version"-1%2B"$OS""$VERSION"_all.deb
else

        echo "OS is not Debian like"
fi

# Install new zabbix_agent version 2
        dpkg -i zabbix-release_"$zabbix_version"*
        apt-get update
        apt-get install zabbix-agent2 -y
        sed -i "s/Server=127.0.0.1/Server=$zabbix_server/" /etc/zabbix/zabbix_agent2.conf
        sed -i "s/ServerActive=127.0.0.1/ServerActive=$zabbix_server/" /etc/zabbix/zabbix_agent2.conf
        sed -i "/Hostname=Zabbix server/d" /etc/zabbix/zabbix_agent2.conf
        service zabbix-agent2 stop
        service zabbix-agent2 start
        rm zabbix-release*
